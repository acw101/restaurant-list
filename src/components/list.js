import { Link } from "gatsby";
import PropTypes from "prop-types";
import React, { useState } from "react";
import yelpLogo from "../images/yelp.png";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

function List(props) {
  const [topThree, setTopThree] = useState(props.listData.slice(0, 3));
  const [remaining, setRemaining] = useState(props.listData.slice(3));
  const onDragEnd = (result) => {
    if (result.destination === null) return;
    if (result.destination.droppableId !== result.source.droppableId) {
      const topThreeClone = Array.from(topThree);
      const remainingClone = Array.from(remaining);
      const sourceIdx = result.source.index;
      const destIdx = result.destination.index;
      if (result.destination.droppableId === "topThree") {
        remainingClone.splice(sourceIdx, 1);
        topThreeClone.splice(destIdx, 0, remaining[sourceIdx])
        remainingClone.splice(0, 0, topThreeClone[3])
        topThreeClone.splice(3, 1)
        console.log(topThreeClone, remainingClone)
        setTopThree(topThreeClone);
        setRemaining(remainingClone);
      } else {
        topThreeClone.splice(sourceIdx, 1);
        remainingClone.splice(destIdx, 0, topThree[sourceIdx]);
        topThreeClone.push(remainingClone[0])
        remainingClone.splice(0, 1)
        setTopThree(topThreeClone);
        setRemaining(remainingClone);
      }
    }
  }
  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="topThree" direction="horizontal">
        {provided => (
          <div ref={provided.innerRef} {...provided.droppableProps} className="overflow-hidden">
            <ListTopItems data={topThree} />
            {provided.placeholder}
          </div>
        )}
      </Droppable>
      <Droppable droppableId="remaining">
        {provided => (
          <div ref={provided.innerRef} {...provided.droppableProps}>
            {remaining.map((data, i) => ListItem(data, i))}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
}

function ListTopItems(props) {
  function TopItemCell(props, index) {
    return (
      <Draggable draggableId={props.id} key={index} index={index}>
        {(provided, snapshot) => {
          return (
            <div
              className="flex flex-col m-2 p-4 items-center h-auto rounded-lg bg-white shadow-md topThree"
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
            >
              <p className="font-bold text-lg text-yellow-500">{index}</p>
              <div className="flex flex-row items-center">
                <img
                  className="h-32 w-32 object-cover mx-2 rounded-lg"
                  src={props.photos[0]}
                />
              </div>
              <div className="flex-grow flex flex-col justify-center items-center">
                <h1 className="text-xl">{props.name}</h1>
                <p className="text-sm font-light text-gray-600">
                  {props.location.city}
                </p>
              </div>
              <a href={props.url}>
                <span className="text-sm align-middle">View on</span>
                <img className="inline h-10" src={yelpLogo} alt="yelp logo" />
              </a>
            </div>
        )}}
      </Draggable>
    );
  }
  return (
    <div className="flex flex-row -m-1">
      {TopItemCell(props.data[0], 0)}
      {TopItemCell(props.data[1], 1)}
      {TopItemCell(props.data[2], 2)}
    </div>
  );
}

function ListItem(props, index) {
  return (
    <Draggable draggableId={props.id} key={index} index={index}>
      {provided => (
        <div
          className="flex flex-row items-center bg-white p-4 m-2 rounded-lg shadow-md"
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <div className="flex flex-row items-center">
            <div className="font-bold text-gray-500">{index}</div>
            <img className="h-20 w-20 mx-2 rounded-lg" src={props.photos[0]} />
          </div>
          <div className="flex-grow flex flex-col justify-center">
            <h1 className="text-xl">{props.name}</h1>
            <p className="text-sm font-light text-gray-600">
              {props.location.city}
            </p>
          </div>
          <a className="middle" href={props.url}>
            <span className="inline-block text-sm align-middle">View on</span>
            <img className="inline-block h-10" src={yelpLogo} alt="yelp logo" />
          </a>
        </div>
      )}
    </Draggable>
  );
}

List.propTypes = {
  listData: PropTypes.array.isRequired
};

export default List;
