import { Link } from "gatsby";
import PropTypes from "prop-types";
import React, { useState } from "react";
import yelpLogo from "../images/yelp.png"

function Detail(props) {
    const topThreeItems = props.listData.slice(0, 3);
    const remainingItems = props.listData.slice(3);
    return (
        <div>
            <ListTopItems data={topThreeItems} />
            {remainingItems.map((data, i) => ListItem(data, i+4))}
        </div>
    );
}

function ListTopItems(props) {
    function TopItemCell(props, rankText) {
        return (
            <div className="flex flex-col m-2 p-4 items-center w-full h-64 rounded-lg bg-white shadow-md hover-enlarge">
                <p className="font-bold text-lg text-yellow-500">{rankText}</p>
                <div className="flex flex-row items-center">
                    <img className="h-20 w-20 mx-2 rounded-lg" src={props.photos[0]}/> 
                </div>
                <div className="flex-grow flex flex-col justify-center items-center">
                    <h1 className="text-xl">{props.name}</h1>
                    <p className="text-sm font-light text-gray-600">{props.location.city}</p>
                </div>
                <a href={ props.url }>
                    <span className="text-sm align-middle">View on</span>
                    <img className="inline h-10" src={yelpLogo} alt="yelp logo" />
                </a>
            </div>
        )
    }
    return (
        <div className="flex flex-row -m-1">
            {TopItemCell(props.data[0], "1st")}
            {TopItemCell(props.data[1], "2nd")}
            {TopItemCell(props.data[2], "3rd")}
        </div>
    )
}


function ListItem(props, rank) {
    return (
        <div className="flex flex-row items-center bg-white p-4 m-2 rounded-lg shadow-md hover-enlarge">
            <div className="flex flex-row items-center">
                <div className="font-bold text-gray-500">{rank + "th"}</div>
                <img className="h-20 w-20 mx-2 rounded-lg" src={props.photos[0]}/> 
            </div>
            <div className="flex-grow flex flex-col justify-center">
                <h1 className="text-xl">{props.name}</h1>
                <p className="text-sm font-light text-gray-600">{props.location.city}</p>
            </div>
            <a className="middle" href={ props.url }>
                <span className="inline-block text-sm align-middle">View on</span>
                <img className="inline-block h-10" src={yelpLogo} alt="yelp logo" />
            </a>
        </div>
    )
}

List.propTypes = {
  listData: PropTypes.array.isRequired
};

export default List;
