import PropTypes from "prop-types";
import React, { useState } from "react";
import yelpLogo from "../images/yelp.png"

const styles = {
    profilePic: {
        maxWidth: "500px",
        height: "100%"
        
    },
    gradientDivider: {
        background: "linear-gradient(to bottom,  rgba(69,72,77,0.66) 0%,rgba(68,71,76,0.66) 1%,rgba(35,37,39,1) 49%,rgba(0,0,0,0.66) 100%)" /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    }

}

function Profile(props) {
    const { profilePic, name, location, intro, followerCnt, followingCnt, lastUpdate } = props.profileData;
    return (
        <div className="h-64 mb-6 bg-white rounded-lg shadow-md flex flex-row">
            <img className="object-cover rounded-lg rounded-r-none" src={profilePic} style={styles.profilePic}/>
            <div className="px-10 py-2">
                <h2 className="text-lg">{name}</h2>
                <h3 className="font-light text-gray-600 pb-1">{location}</h3>
                <p className="pb-2t text-sm text-gray-500">{intro}</p>
                <div className="flex flex-row justify-between py-4">
                    <p className="text-gray-500">Follower: <span>{followerCnt}</span></p>
                    <p className="text-gray-500">Following: <span>{followingCnt}</span></p>
                    <p className="text-gray-500">Last update: <span>{lastUpdate}</span></p>
                </div>
            </div>
        </div>


    )
}

export default Profile;