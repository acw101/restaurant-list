import React from "react";

import Layout from "../components/layout";
import SEO from "../components/seo";
import List from "../components/list";
import Profile from "../components/profile";

const listData = [
    {
        "name": "Ciao! Pizza & Pasta",
        "id": "yQL8SrSETbbCI1U5esVJQw",
        "url": "https://www.yelp.com/biz/ciao-pizza-and-pasta-chelsea-2?adjust_creative=cdT09tcBiv04RZebrDPLIg&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=cdT09tcBiv04RZebrDPLIg",
        "photos": [
            "https://s3-media4.fl.yelpcdn.com/bphoto/2wGzObP-Wb0PPnbA6ZYGFw/o.jpg"
        ],
        "location": {
            "city": "Chelsea"
        }
    },
    {
        "name": "Taipei Cuisine",
        "id": "-NXN4wvFTwOus8wDq96tFA",
        "url": "https://www.yelp.com/biz/taipei-cuisine-quincy?adjust_creative=cdT09tcBiv04RZebrDPLIg&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=cdT09tcBiv04RZebrDPLIg",
        "photos": [
            "https://s3-media1.fl.yelpcdn.com/bphoto/MgRopQLMSZyRvtE66iPmHw/o.jpg"
        ],
        "location": {
            "city": "Quincy"
        }
    },
    {
        "name": "Ganko Ittetsu Ramen",
        "id": "W1kaR_hXKbvaK1bDRhMcZA",
        "url": "https://www.yelp.com/biz/ganko-ittetsu-ramen-brookline?adjust_creative=cdT09tcBiv04RZebrDPLIg&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=cdT09tcBiv04RZebrDPLIg",
        "photos": [
            "https://s3-media3.fl.yelpcdn.com/bphoto/ctVvzOB3EJ4HNJkMlzIyBA/o.jpg"
        ],
        "location": {
            "city": "Brookline"
        }
    },
    {
        "name": "The Daily Catch",
        "id": "uXOVFQraz1va1TrtgiqYTg",
        "url": "https://www.yelp.com/biz/the-daily-catch-boston?adjust_creative=cdT09tcBiv04RZebrDPLIg&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=cdT09tcBiv04RZebrDPLIg",
        "photos": [
            "https://s3-media1.fl.yelpcdn.com/bphoto/PHZBVnfO-DgsyDPt7zQ--w/o.jpg"
        ],
        "location": {
            "city": "Boston"
        }
    },
    {
        "name": "Mala Restaurant",
        "id": "_A6fj7b4qwnmcEEOYOu5vw",
        "url": "https://www.yelp.com/biz/mala-restaurant-boston?adjust_creative=cdT09tcBiv04RZebrDPLIg&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=cdT09tcBiv04RZebrDPLIg",
        "photos": [
            "https://s3-media3.fl.yelpcdn.com/bphoto/mnj6Vpu5xZYPgoBU6MgCQw/o.jpg"
        ],
        "location": {
            "city": "Boston"
        }
    }
]
const profileData = {
    name: "Kevin Wang",
    profilePic: "https://scontent.fbos1-2.fna.fbcdn.net/v/t1.0-9/56395066_10218955098017208_8258585222392578048_n.jpg?_nc_cat=105&_nc_oc=AQlKJJsGCsqSx9GsSAHgnC95-QT_cxt5f9X0kW02FK-OS2GUdj7qeKEQzA_-aFUSBXY&_nc_ht=scontent.fbos1-2.fna&oh=51a8319023c6c99ac173f6beb2375948&oe=5DC4A902",
    location: "Boston, MA",
    intro: "Do labore pariatur minim reprehenderit proident. Laboris laborum enim nostrud labore nostrud eu nostrud. Aliqua sit cupidatat velit consectetur. Consequat reprehenderit minim Lorem sit amet. Ullamco consequat nostrud consequat labore velit adipisicing non irure. Velit aliquip tempor officia proident id occaecat incididunt ea deserunt voluptate. Ea laboris amet nostrud ex sint consequat pariatur quis officia pariatur dolor Lorem.",
    followerCnt: 104,
    followingCnt: 20,
    lastUpdate: "30 days ago"
}

function ListPage() {
  return (
    <Layout>
      <SEO
        title="List"
        keywords={[`gatsby`, `tailwind`, `react`, `tailwindcss`]}
      />
      <Profile profileData={profileData} />
      <List listData={listData}/>
    </Layout>
  );
}

export default ListPage;
